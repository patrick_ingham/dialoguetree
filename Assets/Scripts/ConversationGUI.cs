using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class ConversationGUI : MonoBehaviour {

	public LoadXMLData xmlData;
	public int conversationNumber = 1;
	public int questionNumber = 0;
	
	private DialogueNode dn;
	public Text questionText;
	public Text[] responses = new Text[3];
	public Button[] responseBtns = new Button[3];

	public GameObject imageButton;
	public Canvas playerSelectCanvas;
	public Canvas questionCanvas;

    public Button restartBtn;

	void Start() {
		addNPCSelectionButtons();
	}

	private void addDialogue()
	{
		clearAnswers();
		questionText.text = dn.getQuestion();
		for (int i = 0; i < dn.getResponses().Count; i++)
			{
				responseBtns[i].gameObject.SetActive(true);
				responses[i].text = dn.getResponses()[i];
				addDialogueClickListener(i, responseBtns[i]);
			}
        if(dn.getResponses().Count==0){
            restartBtn.gameObject.SetActive(true);
        }
	}

	private void clearAnswers()
	{
		for (int i = 0; i < responseBtns.Length; i++)
		{
			responseBtns[i].gameObject.SetActive(false);
		}
	}

	private void addDialogueClickListener(int value, Button btn)
	{
		btn.onClick.RemoveAllListeners();
		btn.onClick.AddListener(delegate
		{
			questionNumber = dn.getResponsesLinks()[value];
			dn = getDialogueNode(conversationNumber, questionNumber);
			addDialogue();
		});
	}


	DialogueNode getDialogueNode(int _conversation, int _questionNumber) {
		foreach (DialogueNode _dn in xmlData.conversations) {
			if(_dn.getNodeParent() ==_conversation && _dn.getNodeNumber() == _questionNumber) return _dn;
		}
		return null;
	}

	private void addNPCSelectionButtons()
	{
		int xSteps = 0;
		List<int> names = new List<int>();
		foreach (DialogueNode node in xmlData.conversations)
		{
			if (!names.Contains(node.getNodeParent()))	//check that NPC image only displayed once
			{
				names.Add(node.getNodeParent());
				GameObject ps = Instantiate(imageButton);
				ps.transform.SetParent(FindObjectOfType<Canvas>().transform);
				ps.GetComponent<Image>().sprite = node.getSprite();
				ps.GetComponent<RectTransform>().localPosition = new Vector3((xSteps * 225) - Screen.width / 2 + 175, 0, 0);
				addPlayerSelectClickListener(names.Count, ps.GetComponent<Button>());
				xSteps++;
			}			
		}
	}

	private void addPlayerSelectClickListener(int whichNPC, Button btn)
	{
		btn.onClick.AddListener(delegate
		{
            conversationNumber = whichNPC;
			dn = getDialogueNode(whichNPC, 0);
			playerSelectCanvas.gameObject.SetActive(false);
			questionCanvas.gameObject.SetActive(true);
			
			addDialogue();
		});
	}

    public void restartDialogues() {
        SceneManager.LoadScene(0);
    }
}
